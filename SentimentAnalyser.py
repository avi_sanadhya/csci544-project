import os
import sys
import json
import requests
import urllib

def getSentimentUsingTextProcessing(text):
  url = 'http://text-processing.com/api/sentiment/'
  payload = dict()
  payload['text']=text
  r = requests.post(url, data=payload)
  n = r.json()
  return n

def getSentimentUsingSentigem(text):
  url = 'https://api.sentigem.com/external/get-sentiment'
  payload = dict()
  payload['api-key'] = "a88c17eb954454b43abadc561561512b6qvZHe0L4Gi3sPQd7mfC_gp8OtwzAyaj"
  payload['text'] = text
  responseObj = requests.post(url, data=payload)
  jsonResponse = responseObj.json()
  return jsonResponse["polarity"]

def main():
  if len(sys.argv)!=2:
    print('Usage: python3 sentiment.py text')
    sys.exit(1)
  text=sys.argv[1]
  #print(getSentimentUsingTextProcessing(text))
  print(getSentimentUsingSentigem(text))


if __name__=='__main__':
  main()




