import sys, io, operator
import FileReader
import SemanticSimilarityCalculator
import dependencyParser
import SentimentAnalyser
import SemanticSimilarityCalculator

def getSortedPhrases(semanticPairs, semanticPairCounts):
	sortedPairs = {}
	toBeUsedPairs = {}
	for pair in semanticPairs:
		sortedPairs[semanticPairs[pair]] = float((0.5 * semanticPairCounts[pair[0]]) + (0.5 * SemanticSimilarityCalculator.getSemanticSimilarityMeasure(pair[0], pair[1])))
	for key in sortedPairs:
		if sortedPairs[key] > 0.0:
			toBeUsedPairs[key] = sortedPairs[key]
	toBeUsedPairs = sorted(toBeUsedPairs.items(), key=operator.itemgetter(1))
	return toBeUsedPairs



def getPhrasesListForSemanticSimilarity(mappedDependencies):
	semanticPairs = {}
	semanticPairCounts = {}
	for sentence in mappedDependencies:
		mappedDependencyList = mappedDependencies[sentence]
		for dependencyTuple in mappedDependencyList:
			dependency = dependencyTuple[0]
			if "ADJ" in dependency and "NP" in dependency:
				semanticPairs[(dependency["NP"], dependency["ADJ"])] = dependency["ADJ"] + " - " + dependency["NP"]
				if dependency["NP"] in semanticPairCounts:
					semanticPairCounts[dependency["NP"]] += 1
				else:
					semanticPairCounts[dependency["NP"]] = 1
			else:
				phraseDict = dependencyTuple[0]
				if "NP" in phraseDict:
					phrase = phraseDict["NP"]
				else:
					phrase = phraseDict["ADJ"]
				dependency = dependencyTuple[1]
				noun = dependency[0]
				adj = dependency[1]
				semanticPairs[(noun, adj)] = phrase
				if noun in semanticPairCounts:
					semanticPairCounts[noun] += 1
				else:
					semanticPairCounts[noun] = 1

	sortedPhrases = getSortedPhrases(semanticPairs, semanticPairCounts)
	return sortedPhrases


def getPhrasesSentiments(sortedPhrases):
	reviewSentiments = {}
	reviewSentiments["positive"] = []
	reviewSentiments["negative"] = []
	reviewSentiments["neutral"] = []
	for phraseTuple in sortedPhrases:
		phrase = phraseTuple[0]
		sentiment = SentimentAnalyser.getSentimentUsingSentigem(phrase)
		reviewSentiments[sentiment].append(phrase)

	return reviewSentiments


def mapDependenciesToPhrases(allPhrases, allDependencies):
	mappedDependencies = {}
	for sentence in allPhrases:
		dependencyList = allDependencies[sentence]
		phraseDict = allPhrases[sentence]
		mappedDependencyList = []
		for dependency in dependencyList:
			word1 = dependency[0]
			word2 = dependency[1]
			mappedDependency = {}
			adjpFlag = 0
			npFlag = 0
			flag = 0
			for phrase in phraseDict["ADJP"]:
				if phrase.find(word1) != -1 and phrase.find(word2) != -1:
					if phrase not in mappedDependency.values():
						mappedDependency["ADJ"] = phrase
					flag = 1
					break
				else:
					if phrase.find(word2) != -1:
						if phrase not in mappedDependency.values():
							mappedDependency["ADJ"] = phrase
			if flag == 0:
				for phrase in phraseDict["NP"]:
					if phrase.find(word1) != -1 and phrase.find(word2) != -1:
						if phrase not in mappedDependency.values():
							mappedDependency["ADJ"] = phrase
						break
					else:
						if phrase.find(word1) != -1:
							if phrase not in mappedDependency.values():
								mappedDependency["NP"] = phrase
			
			if mappedDependency not in mappedDependencyList and "ADJ" in mappedDependency:
				mappedDependencyList.append((mappedDependency, dependency))
		mappedDependencies[sentence] = mappedDependencyList
		#print(mappedDependencyList)
	return mappedDependencies


def getAllDependencies(allPhrases):
	allDependencies = {}
	for sentence in allPhrases:
		allDependencies[sentence] = dependencyParser.parse(sentence)
	return allDependencies




def main():
	inputFilePath = sys.argv[1]
	phrasesFilePath = sys.argv[2]
	allPhrases = FileReader.getAllData(inputFilePath, phrasesFilePath)
	allDependencies = getAllDependencies(allPhrases)
	mappedDependencies = mapDependenciesToPhrases(allPhrases, allDependencies)
	sortedPhrases = getPhrasesListForSemanticSimilarity(mappedDependencies)
	phraseSentiments = getPhrasesSentiments(sortedPhrases)
	for sentiment in phraseSentiments:
		phraseList = phraseSentiments[sentiment]
		print(sentiment + ":")
		for phrase in phraseList:
			print("\t" + phrase)




if __name__ == '__main__':
	main()