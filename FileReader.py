import sys, io, json


def getPOSTagsAndSentence(reviews):
	postags = []
	sentences = []
	for review in reviews:
		for sentence in review:
			words = sentence.split(' ')
			sent = []
			posSent = []
			for w in words:
				splitup = w.split('/')
				w = ""
				for i in range(len(splitup) - 1):
					w = w + splitup[i]
				pos = splitup[len(splitup) - 1]
				sent.append(w)
				posSent.append(pos)
			sentences.append((sent, posSent))

	return sentences


		

def getAllReviews(inputFilePath):
	i = 0
	reviews = {}
	with open(inputFilePath, 'r') as inf:
		product = ""
		prevProduct = ""
		tmpDict = []
		products = 1
		for line in inf:
			line = line.replace('\n', '')
			line = line.replace('\r', '')
			if i%3 == 0:
				product = line
				if i == 0:
					prevProduct = product
				if product != prevProduct:
					reviews[product] = getPOSTagsAndSentence(tmpDict)
					prevProduct = product
					tmpDict = []
					reviewInd = 0
					products += 1
			if i%3 == 1:
				tmpDict.append(eval(line))
			i = i + 1
	if products == 1:
		reviews[product] = getPOSTagsAndSentence(tmpDict)
	return reviews

def prepareSentence(sentenceList):
	sentence = ""
	for i in range(len(sentenceList)):
		if i < len(sentenceList) - 1:
			sentence = sentence + sentenceList[i] + " "
		else:
			sentence = sentence + sentenceList[i]
	return sentence

def readData(inputFilePath):
	allSentences = {}
	allReviews = getAllReviews(inputFilePath)
	for product in allReviews:
		reviewList = allReviews[product]
		sentences = []
		for reviewTuple in reviewList:
			sentences.append(prepareSentence(reviewTuple[0]))
		allSentences[product] = sentences
	return allSentences

def readAllPhrases(allSentences, phrasesFilePath):
	i = 0
	allPhrases = {}
	print(len(allSentences))

	with open(phrasesFilePath, 'r') as inf:
		for line in inf:
			tmpDict = eval(line)
			allPhrases[allSentences[i]] = tmpDict
			i += 1
	return allPhrases

def getAllData(inputFilePath, phrasesFilePath):
	allSentences = readData(inputFilePath)
	createSentencesFile(allSentences['Steak \'n Shake'])
	allPhrases = readAllPhrases(allSentences['Steak \'n Shake'], phrasesFilePath)
	return allPhrases

def createSentencesFile(allSentences):
	with open('reviewSentencesFile.txt', 'w+') as ouf:
		for sentence in allSentences:
			ouf.write(sentence + "\n")





def main():
	inputFilePath = sys.argv[1]
	phrasesFilePath = sys.argv[2]
	print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
	allPhrases = getAllData(inputFilePath, phrasesFilePath)
	print(allPhrases)
	print(len(allPhrases))




if __name__ == '__main__':
	main()