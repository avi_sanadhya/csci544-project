from requests import get
import sys

sss_url = "http://swoogle.umbc.edu/SimService/GetSimilarity"

def getSemanticSimilarityMeasure(s1, s2, type='relation', corpus='webbase'):
	try:
		response = get(sss_url, params={'operation':'api','phrase1':s1,'phrase2':s2,'type':'relation','corpus':'gigawords'})
		return float(response.text.strip())
	except:
        #print 'Error in getting similarity for %s: %s' % ((s1,s2), response)
		return 0.0

def main():
	word1 = sys.argv[1]
	word2 = sys.argv[2]
	sim = getSemanticSimilarityMeasure(word1, word2)
	print(sim)


if __name__ == '__main__':
	main()