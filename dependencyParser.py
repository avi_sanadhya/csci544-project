import json
from jsonrpc import ServerProxy, JsonRpc20, TransportTcpIp
from pprint import pprint
import sys


def getPOSTags(wordLists):
  #pprint(wordList)
  POSDictionary=dict()
  for i in range(0,len(wordLists)):
    wordList=wordLists[i]
    word=wordList[0]
    POSTag=wordList[1]['PartOfSpeech']
    POSDictionary[word]=POSTag
  return POSDictionary

def getWordPair(POSDictionary,dependencyList):
  word1=dependencyList[1]
  word2=dependencyList[2]
  tempList=list()
  if 'NN' in POSDictionary[word1]:
    if 'JJ' in POSDictionary[word2]:
      tempList.append(word1)
      tempList.append(word2)
  if 'JJ' in POSDictionary[word1]:
    if 'NN' in POSDictionary[word2]:
      tempList.append(word2)
      tempList.append(word1)
  return tempList
      
  

def getDependencies(POSDictionary,dependencyLists):
  wordPairList=list()
  for i in range(0,len(dependencyLists)):
    dependencyList=dependencyLists[i]
    if (dependencyList[0]=='nsubj' or dependencyList[0]=='amod'):
      wordPair=getWordPair(POSDictionary,dependencyList)
      if len(wordPair)!=0:
        wordPairList.append(wordPair)
  return wordPairList


def parse(text):
  server = ServerProxy(JsonRpc20(),TransportTcpIp(addr=("127.0.0.1", 3624)))
  try:
    jsonResult=json.loads(server.parse(text))
  except:
    return list()
  #jsonResult2=json.loads(server.ner(text))
  POSDictionary=getPOSTags(jsonResult['sentences'][0]['words'])
  dependencies=getDependencies(POSDictionary,jsonResult['sentences'][0]['dependencies'])
  #pprint(jsonResult['sentences'][0]['dependencies'])
  #pprint(jsonResult2)
  #print('++++++++++++++++++++++++++++++++++++++++++++++++++++++')
  #print(dependencies)
  #resultDictionary={}
  #pprint(POSDictionary)
  return dependencies
  
def main():
  if len(sys.argv) == 2:
    inputText=sys.argv[1]
  else:
    inputText="World is beautiful"
  result=parse(inputText)
  pprint(result)
  
if __name__ == '__main__':
  main()
