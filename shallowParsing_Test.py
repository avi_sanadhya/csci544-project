import sys
import MBSP
import re


#Dictionary to hold product title and review text
reviewDict={}
#Dictionary to hold processed reviews
pReviewDict={}

#method to load the file/reviews in global data structure
def loadFile(inputFile):
    FlagT,FlagR=False,False
    with open(inputFile) as fin:
        for line in fin:
            line=line.strip()
            if(line.startswith('product/title')):
                title=line.split(':')[1]
                if title not in reviewDict:
                    reviewDict[title]=[]
                FlagT=True
            if(line.startswith('review/text')):
                review=line.split(':')[1]
                FlagR=True
            if FlagR and FlagT:
                reviewDict[title].append(review)
                FlagR,FlagT=False,False

#method to replace pronoun(it) with its noun
def preProcess():
    for product in reviewDict.keys():
        pReviewDict[product]=[]
        currentSubj=product
        reviewList=reviewDict[product]
        for review in reviewList:
            newSent=''
            tagged=MBSP.parse(review)
            s=MBSP.split(tagged)
            for i in range (0,len(s)):
                for chunk in s[i].chunks:
                    if chunk.role and 'SBJ' == chunk.role and chunk.string.lower() != "It".lower() and chunk.string.lower() != "I".lower():
                        currentSubj=chunk.string
                newSent=newSent+re.sub(r"\b(it|It|It's|it's)\b"," "+currentSubj+" ",s[i].string)
                currentSubj=product
            pReviewDict[product].append(str(newSent))

# Method to Pos tag individual reviews after pre processing
def posTagReviews():
    for product in pReviewDict.keys():
        reviewList=pReviewDict[product]
        for review in reviewList:
            tagged=str(MBSP.tag(review))
            print(product)
            print(tagged.split("\n"))
            print("####")

# Method to remove determiners and pronouns not required
def stripPhrase(phrase):
    prePhrase=phrase.split(' ',1)
    posTag=str(MBSP.tag(prePhrase[0])).split("/")[1]
    if(posTag == "PRP" or posTag == "DT" or posTag== "PRP$" or posTag=="FW" ):
        toAppend=''
    else:
        toAppend=prePhrase[0]
    if len(prePhrase) > 1:
        newPhrase=toAppend+" "+prePhrase[1]
    else:
        newPhrase=toAppend
    return newPhrase


# Method to Extract individual noun and adjective phrases form the reviews
def doParsing(text):
    s = MBSP.chunk(text)
    s = MBSP.split(s)
    for i in range (0,len(s)):
        npList=[]
        adjpList=[]
        phraseDict={}
        for chunk in s[i].chunks:
            if 'NP' in chunk.type:
                npList.append(str(stripPhrase(chunk.string)))
            if 'ADJP' in chunk.type:
                adjpList.append(str(stripPhrase(chunk.string)))
        phraseDict['NP']=filter(None,npList)
        phraseDict['ADJP']=filter(None,adjpList)
        print(phraseDict)

def parseFile():
    for product in pReviewDict.keys():
        reviewList=pReviewDict[product]
        for review in reviewList:
            doParsing(review)


def main():
    input= sys.argv[1]
    loadFile(input)
    preProcess()
    # posTagReviews()
    parseFile()


if __name__ == "__main__":
    # execute only if run as a script
    main()
