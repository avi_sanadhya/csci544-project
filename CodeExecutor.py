import sys, io
import FileReader
import SemanticSimilarityCalculator

allReviews = {}
segmentedReviewsPosTags = {}
segmentedReviewsPhrases = {}
semanticMeasures = {}

def readData(inputFilePath):
	allReviews = FileReader.getAllReviews(inputFilePath)
	#print(allReviews)
	for product in allReviews:
		reviews = allReviews[product]
		tmpListPOS = []
		tmpListPhr = []
		for i in reviews:
			review = allReviews[product][i]
			taggedData = FileReader.getPOSandPhraseStructure(review)
			tmpListPOS.append(taggedData[0])
			tmpListPhr.append(taggedData[1])
		segmentedReviewsPosTags[product] = tmpListPOS
		segmentedReviewsPhrases[product] = tmpListPhr
	#print(segmentedReviewsPosTags)
	#print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
	#print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
	#print(segmentedReviewsPhrases)

def generateNounADJPPhrasePairs(nPhrases, adjPhrases):
	pairs = {}
	for nPhrase in nPhrases:
		for adjPhrase in adjPhrases:
			pairs[(nPhrase, adjPhrase)] = float(0.0)
	return pairs

def getSemanticMeasures():
	for product in segmentedReviewsPhrases:
		for review in segmentedReviewsPhrases[product]:
			adjPhrases = []
			nPhrases = []
			for phrase in review:
				phraseType = review[phrase]
				if phraseType == 'NP':
					nPhrases.append(phrase)
				elif phraseType == 'ADJP':
					adjPhrases.append(phrase)

			phrasePairs = generateNounADJPPhrasePairs(nPhrases, adjPhrases)
			for pair in phrasePairs:
				phrasePairs[pair] = SemanticSimilarityCalculator.getSemanticSimilarityMeasure(pair[0], pair[1])
			print(phrasePairs)


def main():
	inputFilePath = sys.argv[1]
	readData(inputFilePath)
	getSemanticMeasures()



if __name__ == '__main__':
	main()