import os
import sys
import json
import requests
import urllib

def getSentiment(text):
  url = 'http://text-processing.com/api/sentiment/'
  payload = dict()
  payload['text']=text
  r = requests.post(url, data=payload)
  n = r.json()
  return n

def main():
  if len(sys.argv)!=2:
    print('Usage: python3 sentiment.py text')
    sys.exit(1)
  text=sys.argv[1]
  print(getSentiment(text))


if __name__=='__main__':
  main()




