import json
import shallowParsing_Test
import MBSP
import re
import sys

#dictionary to hold all restaurant id and their name
restaurantDict={}
#Dictionary to hold restaurant title and review text
reviewDict={}
#Dictionary to hold processed reviews
pReviewDict={}

#method to load the file/reviews in global data structure
def loadFile(inputFile):
    count=1
    with open(inputFile) as fin:
        for line in fin:
            line=line.strip()
            reviews=json.loads(line)
            count=count+1
            if restaurantDict[reviews["business_id"]] not in reviewDict:
                    reviewDict[restaurantDict[reviews["business_id"]]]=[]
            reviewDict[restaurantDict[reviews["business_id"]]].append(reviews["text"])
            # currenlty reading only first 150 reviews as file is big
            if(count > 100 ):
                break

# method to get restaurant id to name mapping
def getMapping(buisnessFile):
    with open(buisnessFile) as fin:
        for line in fin:
            line=line.strip()
            businessDict=json.loads(line)
            if businessDict["business_id"] not in restaurantDict:
                restaurantDict[businessDict["business_id"]] = businessDict["name"]

#method to replace pronoun(it) with its noun
def preProcess(id):
        product=restaurantDict[id]
        pReviewDict[product]=[]
        currentSubj=product
        reviewList=reviewDict[product]
        for review in reviewList:
            newSent=''
            tagged=MBSP.parse(review)
            s=MBSP.split(tagged)
            for i in range (0,len(s)):
                for chunk in s[i].chunks:
                    if chunk.role and 'SBJ' == chunk.role and chunk.string.lower() != "It".lower() and chunk.string.lower() != "I".lower():
                        currentSubj=chunk.string
                newSent=newSent+re.sub(r"\b(it|It|It's|it's)\b"," "+currentSubj+" ",s[i].string)
                currentSubj=product
            pReviewDict[product].append(str(newSent))

# Method to Pos tag individual reviews after pre processing
def posTagReviews(id):
        reviewList=pReviewDict[restaurantDict[id]]
        for review in reviewList:
            tagged=str(MBSP.tag(review))
            print(restaurantDict[id])
            print(tagged.split("\n"))
            print("####")


# method to get reviews of a particular id
def getReviews(id):
    reviewList=pReviewDict[restaurantDict[id]]
    for review in reviewList:
        text=str(review).strip()
        print(shallowParsing_Test.doParsing(text))



def main():
    inputReview = "yelpData/yelp_academic_dataset_review.json"
    inputBusiness = "yelpData/yelp_academic_dataset_business.json"
    command=sys.argv[1]
    getMapping(inputBusiness)
    loadFile(inputReview)
    preProcess("b9WZJp5L1RZr4F1nxclOoQ")
    if(command=="pos"):
        posTagReviews("b9WZJp5L1RZr4F1nxclOoQ")
    else:
        getReviews("b9WZJp5L1RZr4F1nxclOoQ")


if __name__ == "__main__":
    # execute only if run as a script
    main()
